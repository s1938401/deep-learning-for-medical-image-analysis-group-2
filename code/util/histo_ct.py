import os

TRAIN_PATH = "../../Dataset/MHA/Training"
TEST_PATH = "../../Dataset/MHA/Test"

if not os.path.exists(TRAIN_PATH):
    print("Please update your data path to an existing folder.")
elif not '.mha' in os.listdir(TRAIN_PATH)[0]:
    print("The folder exists there are no MHA files found.")
else:
    print("Congrats! You selected the correct folder :)")

# from torch.utils.data import Dataset
from monai.data import Dataset
import torch
import itk
from matplotlib import pyplot as plt
# import time
import numpy as np
# import shutil
from scipy.ndimage.filters import gaussian_filter

class SegmentationDataset(Dataset):
	def __init__(self, imagePaths, regImg_ext, transforms=None):
		# store the image and mask filepaths, and augmentation
		# transforms
		self.imagePaths = imagePaths
		self.images = self.load_mha_images(imagePaths,['CTI',regImg_ext,'R'])
		self.loadedImages = self.images['CTI']
		self.loadedMask = self.images['R']
		self.transforms = transforms

	def load_mha_images(self, paths, ext):
		img = dict()
		counter_reference = 0
		for i in paths:
			if len(ext)>1:
				R_image = np.clip(itk.GetArrayFromImage(itk.imread(i+""+ext[2]+".mha")),0,1)
				CTI_image = itk.GetArrayFromImage(itk.imread(i+""+ext[0]+".mha"))
				CTAI_image = itk.GetArrayFromImage(itk.imread(i+""+ext[1]+".mha"))
				if not (len(CTI_image) == len(CTAI_image) == len(R_image)):
					raise ValueError("Lengths (slices) of "+i+" are not the same!")
				for j in range(len(CTI_image)):
					if True:
					
						if ext[0] in img:
							img[ext[0]].append(torch.stack((torch.FloatTensor(CTI_image[j]),torch.FloatTensor(CTAI_image[j]))))
							img[ext[2]].append(torch.FloatTensor(R_image[j]))
						else:
							img[ext[0]] = [torch.stack((torch.FloatTensor(CTI_image[j]),torch.FloatTensor(CTAI_image[j])))]
							img[ext[2]] = [torch.FloatTensor(R_image[j])]
		return img

	def __len__(self):
		# return the number of total samples contained in the dataset
		return len(self.images['CTI'])

	def __getitem__(self, idx):
		# grab the image path from the current index
		image = self.loadedImages[idx]
		# load the image from disk, swap its channels from BGR to RGB,
		# and read the associated mask from disk in grayscale mode
		mask = self.loadedMask[idx]
		# check to see if we are applying any transformations
		if self.transforms is not None:
			# apply the transformations to both image and its mask
			image = self.transforms(image)
			mask = self.transforms(mask)
		# return a tuple of the image and its mask
		return (image, mask)

train_image_CTI_paths = list(filter(lambda x: 'CTI' in x,os.listdir(TRAIN_PATH)))
train_image_paths = []
for i in train_image_CTI_paths:
    train_image_paths.append(TRAIN_PATH+"/"+i[:6])

def NormalizeData(data):
    return (data - np.min(data)) / (np.max(data) - np.min(data))
train_dataset = SegmentationDataset(train_image_paths,'CTAI_0')
print(len(train_dataset))

# ct_sum = train_dataset[0][0][0].numpy()
# cta_sum = train_dataset[0][0][1].numpy()
# for i in range(1,len(train_dataset)):
#     ct_sum += train_dataset[i][0][0].numpy()
#     cta_sum += train_dataset[i][0][1].numpy()

fig, axes = plt.subplots(3,2)
axes[0][0].hist(train_dataset[1][0][0].numpy(),density=True) # [i][0][0] CT
axes[0][1].hist(train_dataset[1][0][1].numpy(),density=True) # [i][0][1] CTA
axes[1][0].hist(train_dataset[200][0][0].numpy(),density=True) # [i][0][0] CT
axes[1][1].hist(train_dataset[200][0][1].numpy(),density=True) # [i][0][1] CTA
axes[2][0].hist(train_dataset[300][0][0].numpy(),density=True) # [i][0][0] CT
axes[2][1].hist(train_dataset[300][0][1].numpy(),density=True) # [i][0][1] CTA
plt.show()
