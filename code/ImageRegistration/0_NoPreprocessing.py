import itk
import itkwidgets
import numpy as np
import os

TRAIN_PATH = "../../Dataset/MHA/Training"
TEST_PATH = "../../Dataset/MHA/Test"

if not os.path.exists(TRAIN_PATH):
    print("Please update your data path to an existing folder.")
elif not '.mha' in os.listdir(TRAIN_PATH)[0]:
    print("The folder exists there are no MHA files found.")
else:
    print("Congrats! You selected the correct folder :)")

img_CTAI_list = list(filter(lambda x: 'CTI' in x, os.listdir(TRAIN_PATH)))
img_list = []
for i in img_CTAI_list:
    img_list.append(i[:6])

for i in img_list:
    fixed_image = itk.imread(TRAIN_PATH+'/'+i+'CTI.mha', itk.F)
    moving_image = itk.imread(TRAIN_PATH+'/'+i+'CTAI.mha', itk.F)

    parameter_object = itk.ParameterObject.New()
    default_rigid_parameter_map = parameter_object.GetDefaultParameterMap('rigid')
    parameter_object.AddParameterMap(default_rigid_parameter_map)

    result_image, result_transform_parameters = itk.elastix_registration_method(
        fixed_image, moving_image,
        parameter_object=parameter_object,
        log_to_console=True)

    # Load Elastix Image Filter Object
    elastix_object = itk.ElastixRegistrationMethod.New(fixed_image, moving_image)
    # elastix_object.SetFixedImage(fixed_image)
    # elastix_object.SetMovingImage(moving_image)
    elastix_object.SetParameterObject(parameter_object)

    # Set additional options
    elastix_object.SetLogToConsole(False)

    # Update filter object (required)
    elastix_object.UpdateLargestPossibleRegion()

    # Results of Registration
    result_image = elastix_object.GetOutput()
    result_transform_parameters = elastix_object.GetTransformParameterObject()

    itk.imwrite(result_image,TRAIN_PATH+'/'+i+'CTAI_0.mha')